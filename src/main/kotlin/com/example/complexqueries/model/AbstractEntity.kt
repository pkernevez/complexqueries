package com.example.complexqueries.model

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.util.*
import javax.persistence.MappedSuperclass
import javax.persistence.Temporal
import javax.persistence.TemporalType

@MappedSuperclass
abstract class AbstractEntity<T> : AbstractJpaPersistable<T>() {

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    lateinit var createDate: Date

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    lateinit var updateDate: Date
}
