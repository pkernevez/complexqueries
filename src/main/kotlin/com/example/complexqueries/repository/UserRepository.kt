package com.example.complexqueries.repository

import com.example.complexqueries.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Int> {
    fun findBySubject(subject: String): User?
}
