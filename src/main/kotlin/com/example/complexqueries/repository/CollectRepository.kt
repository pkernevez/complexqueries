package com.example.complexqueries.repository

import com.example.complexqueries.model.Collect
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface CollectRepository : JpaRepository<Collect, Int> {


    @Query("SELECT * FROM COLLECT c JOIN USER u ON c.REQUESTER_ID = u.ID WHERE c.STATUS = 'VALIDATED' AND u.AREA_ID " +
            "IN (SELECT uca.COLLECTION_AREA_ID FROM USER_COLLECTION_AREA uca WHERE uca.USER_ID = :collector_id)",
            nativeQuery = true)
    fun findValidatedCollectsInCollectorArea(collector_id: Int): List<Collect>

    @Query("SELECT c FROM Collect c JOIN User u ON c.requester = u WHERE c.status = 'VALIDATED' AND " +
            "u.area IN (SELECT a FROM User collector  JOIN collector.collectionArea a WHERE collector.id = :collector_id)")
    fun findValidatedCollectsInCollectorAreaJPQL(collector_id: Int): List<Collect>



}
