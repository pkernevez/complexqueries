package com.example.complexqueries.repository

import com.example.complexqueries.model.*
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Join
import javax.persistence.criteria.Root


@Service
class CollectCriteriaDao(var em: EntityManager) {

    fun findValidatedCollectsInCollectorAreaCriteria(collector_id: Int): List<Collect> {

        val cb: CriteriaBuilder = em.criteriaBuilder
        val cq: CriteriaQuery<Collect> = cb.createQuery(Collect::class.java)

        val collect: Root<Collect> = cq.from(Collect::class.java)

//        @Query("SELECT c FROM Collect c JOIN User u ON c.requester = u WHERE c.status = 'VALIDATED' AND " +
//                "u.area IN (SELECT a FROM User collector  JOIN collector.collectionArea a WHERE collector.id = :collector_id)")

        val joinUser: Join<Collect, User> = collect.join(Collect_.requester)

        val collectorAreas = cq.subquery(Area::class.java)
        val subRoot = collectorAreas.from(User::class.java)
        val joinAreas = subRoot.join(User_.collectionArea)
        collectorAreas.select(joinAreas)
        collectorAreas.where(cb.equal(joinAreas.get(User_.id), collector_id))


        cq.select(collect)
            .where(
                cb.and(
                    cb.equal(collect.get(Collect_.status), CollectStatus.VALIDATED),
                    joinUser.get(User_.area).`in`(collectorAreas)
                )
            )

        return em.createQuery(cq).resultList
    }

}
