package com.example.complexqueries

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ComplexqueriesApplication

fun main(args: Array<String>) {
    runApplication<ComplexqueriesApplication>(*args)
}
