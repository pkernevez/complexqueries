-- noinspection SqlNoDataSourceInspectionForFile

INSERT INTO AREA (id, name, zipcode) VALUES (-1, 'New York', '1174')
INSERT INTO AREA (id, name, zipcode) VALUES (-2, 'Boston', '1174')

INSERT INTO USER (id, subject) VALUES (-1, 'anna')
INSERT INTO USER (id, subject, area_id) VALUES (-2, 'leo', -1)
INSERT INTO USER (id, subject, area_id) VALUES (-3, 'joao', -2)
INSERT INTO USER (id, subject, area_id) VALUES (-4, 'beatrix', -1)
INSERT INTO USER (id, subject) VALUES (-5, 'anyuser')

INSERT INTO USER_COLLECTION_AREA (user_id, collection_area_id) VALUES (-1, -1)


